//
//  NightView.swift
//  NosferatuNightCounter
//
//  Created by Luc-Olivier MERSON on 28/05/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit

public class NightView: UIView {

    private let label = UILabel()
    private let imageView = UIImageView()

    public var nightType: NightType?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(imageView)
        addSubview(label)

        backgroundColor = .clear

        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 20)
        label.textAlignment = .center

        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 5
        imageView.contentMode = .center

    }

    private func initConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
        }
        label.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
        }
    }

    public func fill(nightType: NightType) {
        self.nightType = nightType

        label.text = nightType.name
        label.textColor = nightType.textColor

        imageView.image = nightType.image
        imageView.backgroundColor = nightType.color
        imageView.layer.borderColor = nightType.cornerColor?.cgColor
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.frame.size.height / 2
    }
}
