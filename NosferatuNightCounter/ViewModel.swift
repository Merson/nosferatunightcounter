//
//  ViewModel.swift
//  NosferatuNightCounter
//
//  Created by Luc-Olivier MERSON on 28/05/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa
import RxDataSources

class ViewModel {

    public let nightCountRelay = PublishRelay<Int>()
    public let nightTypesRelay = PublishRelay<[NightType]>()

    private let disposeBag = DisposeBag()

    init() {
        initRx()
    }

    private func initRx() {
        nightCountRelay.map { (count) -> [NightType] in
            var tmp: [NightType] = Array(repeating: .night, count: count)
            tmp.append(.dawn)
            tmp.shuffle()
            tmp.insert(.none, at: 0)
            print("tmp = \(tmp)")
            return tmp
        }.bind(to: nightTypesRelay).disposed(by: disposeBag)
    }
}
