//
//  CollectionViewCell.swift
//  NosferatuNightCounter
//
//  Created by Luc-Olivier MERSON on 28/05/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import RxSwift
import Reusable

class CollectionViewCell: UICollectionViewCell, Reusable {

    public var disposeBag = DisposeBag()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    private func commonInit() {
    }
}
