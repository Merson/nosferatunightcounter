//
//  NightCollectionViewCell.swift
//  NosferatuNightCounter
//
//  Created by Luc-Olivier MERSON on 28/05/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit

class NightCollectionViewCell: CollectionViewCell {

    public let view = NightView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initUi()
        initConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func fill(nightType: NightType) {
        view.fill(nightType: nightType)
    }

    private func initUi() {
        clipsToBounds = false
        contentView.addSubview(view)
    }

    private func initConstraints() {
        view.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
