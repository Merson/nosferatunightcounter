//
//  NightType.swift
//  NosferatuNightCounter
//
//  Created by Luc-Olivier MERSON on 01/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import AVFoundation

public enum NightType {
    case none
    case night
    case dawn

    var name: String? {
        switch self {
        case .none:
            return "C'est au joueur qui a le pieux de commencer"
        case .night:
            return "Nuit"
        case .dawn:
            return "Aube"
        }
    }

    var image: UIImage? {
        switch self {
        case .none:
            return nil
        case .night:
            return #imageLiteral(resourceName: "night")
        case .dawn:
            return #imageLiteral(resourceName: "dawn")
        }
    }

    var color: UIColor? {
        switch self {
        case .none:
            return .black
        case .night:
            return .red
        case .dawn:
            return .yellow
        }
    }

    var textColor: UIColor? {
        switch self {
        case .none, .night, .dawn:
            return .white
        }
    }

    var cornerColor: UIColor? {
        switch self {
        case .none:
            return .black
        case .night:
            return .red
        case .dawn:
            return .green
        }
    }

    var sound: URL? {
        switch self {
        case .none:
            return nil
        case .night:
            let pathToSound = Bundle.main.path(forResource: "night", ofType: "m4a")!
            let url = URL(fileURLWithPath: pathToSound)
            return url
        case .dawn:
            let pathToSound = Bundle.main.path(forResource: "dawn", ofType: "m4a")!
            let url = URL(fileURLWithPath: pathToSound)
            return url
        }
    }
}
