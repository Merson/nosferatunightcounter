//
//  ViewController.swift
//  NosferatuNightCounter
//
//  Created by Luc-Olivier MERSON on 28/05/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa
import RxDataSources
import AVFoundation

class ViewController: UIViewController, UICollectionViewDelegate {

    private let viewModel = ViewModel()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playerCountTextField: UITextField!
    @IBOutlet weak var removeNightButton: UIButton!
    @IBOutlet weak var addNightButton: UIButton!
    @IBOutlet weak var nextNightButton: UIButton!
    @IBOutlet weak var nightCountLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!

    public lazy var nightCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let bounds = UIScreen.main.bounds
        let total = bounds.width
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: total, height: total)
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()

    private var audioPlayer: AVAudioPlayer?

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        initUI()
        initConstraints()
        initRx()
    }

    private func initUI() {
        view.addSubview(nightCollectionView)

        nightCollectionView.delegate = self
        nightCollectionView.showsVerticalScrollIndicator = false
        nightCollectionView.showsHorizontalScrollIndicator = false
        nightCollectionView.isPagingEnabled = true
        nightCollectionView.isScrollEnabled = false
        nightCollectionView.backgroundColor = .clear
        nightCollectionView.register(NightCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: NightCollectionViewCell.self))

        removeNightButton.layer.cornerRadius = 18
        addNightButton.layer.cornerRadius = 18
        nextNightButton.layer.cornerRadius = 36

        removeNightButton.alpha = 0
        nightCountLabel.alpha = 0
        addNightButton.alpha = 0
        nightCollectionView.alpha = 0
        nextNightButton.alpha = 0
        resetButton.alpha = 0
    }

    private func initConstraints() {
        nightCollectionView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(nightCollectionView.snp.width)
        }
    }

    private func initRx() {

        // On scroll vers la prochaine carte nuit
        nextNightButton.rx.tap.withUnretained(self).bind { (me, _) in
            me.scrollToNextCell()
        }.disposed(by: disposeBag)

        // On reset
        resetButton.rx.tap.withLatestFrom(viewModel.nightCountRelay).bind(to: viewModel.nightCountRelay).disposed(by: disposeBag)

        // On réactive le bouton nuit suivante si on reset
        resetButton.rx.tap.withUnretained(self).bind { (me, _) in
            me.nextNightButton.isEnabled = true
        }.disposed(by: disposeBag)

        playerCountTextField
            .rx
            .text
            .distinctUntilChanged()
            .unwrap()
            .observeOn(MainScheduler.instance)
            .withUnretained(self)
            .do(onNext: { (me, str) in
                me.hideIfNeeded(needed: str == "")
            })
            .map { Int($0.1) }
            .unwrap()
            .bind(to: viewModel.nightCountRelay)
            .disposed(by: disposeBag)

        viewModel.nightCountRelay.map { "Il y a \($0) nuits" }.bind(to: nightCountLabel.rx.text).disposed(by: disposeBag)

        removeNightButton.rx.tap.withLatestFrom(viewModel.nightCountRelay).map { $0 - 1 }.bind(to: viewModel.nightCountRelay).disposed(by: disposeBag)

        addNightButton.rx.tap.withLatestFrom(viewModel.nightCountRelay).map { $0 + 1 }.bind(to: viewModel.nightCountRelay).disposed(by: disposeBag)

        viewModel.nightTypesRelay.withUnretained(self).bind { (me, nightTypes) in
            me.fill(nightTypes: nightTypes)
        }.disposed(by: disposeBag)

        nightCollectionView.rx.willDisplayCell.withUnretained(self).bind { (arg0) in
            let (me, (cell, indexPath)) = arg0
            me.activateCell(cell: cell, indexPath: indexPath)
        }.disposed(by: disposeBag)
    }

    private func fill(nightTypes: [NightType]) {
        nightCollectionView.dataSource = nil
        Observable.just(nightTypes).bind(to: nightCollectionView.rx.items(
            cellIdentifier: String(describing: NightCollectionViewCell.self),
            cellType: NightCollectionViewCell.self)) { _, data, cell in
                cell.fill(nightType: data)
        }.disposed(by: disposeBag)
    }

    private func hideIfNeeded(needed: Bool) {
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn], animations: {
            self.removeNightButton.alpha = needed ? 0 : 1
            self.nightCountLabel.alpha = needed ? 0 : 1
            self.addNightButton.alpha = needed ? 0 : 1
            self.nightCollectionView.alpha = needed ? 0 : 1
            self.nextNightButton.alpha = needed ? 0 : 1
            self.resetButton.alpha = needed ? 0 : 1
        })
    }

    private func activateCell(cell: UICollectionViewCell, indexPath: IndexPath) {
        if let cell = cell as? NightCollectionViewCell,
            let nightType = cell.view.nightType {
            do {
                let playerCount = Int(playerCountTextField.text ?? "") ?? 0
                if indexPath.row == playerCount && nightType == .night {
                    let pathToSound = Bundle.main.path(forResource: "nodawn", ofType: "m4a")!
                    let url = URL(fileURLWithPath: pathToSound)
                    audioPlayer = try AVAudioPlayer(contentsOf: url)
                    audioPlayer?.play()
                    nextNightButton.isEnabled = false
                } else {
                    if let sound = nightType.sound {
                        audioPlayer = try AVAudioPlayer(contentsOf: sound)
                        audioPlayer?.play()
                    }
                }
                if nightType == .dawn {
                    nextNightButton.isEnabled = false
                }
            } catch {
            }
        }
    }
}

extension ViewController {
    func scrollToNextCell() {

        //get cell size
        let cellSize = view.frame.size

        //get current content Offset of the Collection view
        let contentOffset = nightCollectionView.contentOffset

        if nightCollectionView.contentSize.width <= nightCollectionView.contentOffset.x + cellSize.width {
            let r = CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            nightCollectionView.scrollRectToVisible(r, animated: true)
        } else {
            let r = CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            nightCollectionView.scrollRectToVisible(r, animated: true);
        }
    }
}

/*extension ViewController: UICollectionViewDataSource {
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 return 10
 }

 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NightCollectionViewCell.self), for: indexPath) as? NightCollectionViewCell else {
 return UICollectionViewCell()
 }
 cell.fill(nightType: .night)
 return cell
 }
 }*/
